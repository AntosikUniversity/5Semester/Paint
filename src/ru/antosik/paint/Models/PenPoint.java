package ru.antosik.paint.Models;

import java.awt.*;
import java.awt.geom.Line2D;

public class PenPoint {
    private Color color;
    private int thickness;
    private Point coordinates;

    public PenPoint(Point coordinates, Color color, int thickness) {
        this.color = color;
        this.thickness = thickness;
        this.coordinates = coordinates;
    }

    public Color getColor() {
        return color;
    }

    public int getThickness() {
        return thickness;
    }

    public Point getCoordinates() {
        return coordinates;
    }

    public void draw(Graphics2D g2d) {
        Line2D point = new Line2D.Float(coordinates.x, coordinates.y, coordinates.x, coordinates.y);

        g2d.setStroke(new BasicStroke(thickness, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2d.setColor(color);
        g2d.fill(point);
        g2d.draw(point);
    }
}
