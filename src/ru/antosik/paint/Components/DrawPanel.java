package ru.antosik.paint.Components;

import ru.antosik.paint.Models.PenPoint;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class DrawPanel extends JPanel {
    private List<PenPoint> points = new ArrayList<>();
    private int thickness;

    public Color getColor() {
        return color;
    }

    private Color color;

    public DrawPanel() {
        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(600, 300));
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                addPoint(e.getPoint());
            }
            @Override
            public void mousePressed(MouseEvent e) {
                addPoint(e.getPoint());
            }
        });

        color = Color.BLACK;
        thickness = 10;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        for (PenPoint point : points) {
            point.draw(g2);
        }
    }

    public void setColor(Color newColor) {
        color = newColor;
    }

    private void addPoint(Point coordinates) {
        points.add(new PenPoint(coordinates, color, thickness));
        repaint();
    }

    public void setThickness(int newThickness) {
        thickness = newThickness;
    }
}
