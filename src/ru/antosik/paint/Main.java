package ru.antosik.paint;

import ru.antosik.paint.Windows.Paint;

public class Main {
    public static void main(String[] args) {
        Paint dialog = new Paint();
        dialog.setTitle("Paint");
        dialog.setDefaultCloseOperation(dialog.DISPOSE_ON_CLOSE);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
