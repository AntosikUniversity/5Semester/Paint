package ru.antosik.paint.Windows;

import com.intellij.uiDesigner.core.GridConstraints;
import ru.antosik.paint.Components.DrawPanel;

import javax.swing.*;
import java.awt.*;

public class Paint extends JDialog {
    private JPanel contentPane;
    private JButton selectColorButton;
    private JSlider penThickness;
    private DrawPanel drawPanel;
    private JButton aboutDialogButton;

    public Paint() {
        setPreferredSize(new Dimension(600, 500));
        setContentPane(contentPane);
        setModal(true);

        createUIComponents();
        initUIComponents();
    }

    private void initUIComponents() {
        selectColorButton.addActionListener(e -> drawPanel.setColor(JColorChooser.showDialog(this, "Choose pen color", drawPanel.getColor())));

        penThickness.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            if (!source.getValueIsAdjusting()) {
                drawPanel.setThickness(source.getValue());
            }
        });

        aboutDialogButton.addActionListener(e -> JOptionPane.showMessageDialog(null, aboutMessage));
    }

    private void createUIComponents() {
        drawPanel = new DrawPanel();
        contentPane.add(drawPanel, new GridConstraints());
        pack();
    }

    private static final String aboutMessage =
            "Paint\n" +
                    "Можно рисовать пером, выбирать его цвет и толщину линии. \n\n" +
                    "Made by Антон Григорьев \n" +
                    "Группа 8-3ИНТ-3ДБ-035-15";
}
